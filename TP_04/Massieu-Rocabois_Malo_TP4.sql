/*

Compagnie(
	idComp (1),
	nomComp,
	pays,
	estLowCost
)

Pilote(
	idPilote (1),
	nomPilote,
	nbHVol,
	compPil = @Compagnie(idComp)
)

TypeAvion(
	idTypeAvion (1),
	nbPassagers
)

Qualification(
	unPilote = @Pilote(idPilote) (1),
	unTypeAvion = @TypeAvion(idTypeAvion) (1)
)

Avion(
	idAvion (1),
	leTypeAvion = @TypeAvion(idTypeAvion) (NN),
	compAv = @Compagnie(idComp) (NN)
)

ATravaillePour (
	lePilote = @Pilote(idPilote) (1),
	laComp = @Compagnie(idComp) (1)
)

*/

-- DIVISION 

	-- 1. les noms des pilotes ayant travaillé pour toutes les compagnies.

	SELECT DISTINCT nomPilote
	FROM Pilote
	WHERE NOT EXISTS (
		SELECT idComp
		FROM Compagnie
		MINUS
		SELECT laComp
		FROM ATravaillePour
		WHERE idPilote = lePilote
	);

	SELECT DISTINCT nomPilote
	FROM Pilote, ATravaillePour
	WHERE idPilote = lePilote
	GROUP BY nomPilote
	HAVING COUNT(DISTINCT laComp) = (
		SELECT COUNT(idComp)
		FROM Compagnie
	);

	-- 2. les noms des pilotes ayant travaillé pour toutes les compagnies low cost.

	SELECT DISTINCT nomPilote
	FROM Pilote
	WHERE NOT EXISTS (
		SELECT idComp
		FROM Compagnie
		WHERE estLowCost = 1
		MINUS
		SELECT laComp
		FROM ATravaillePour
		WHERE idPilote = lePilote
	);

	SELECT DISTINCT nomPilote
	FROM Pilote, ATravaillePour, Compagnie
	WHERE idPilote = lePilote
	AND laComp = idComp
	AND estLowCost = 1
	GROUP BY nomPilote
	HAVING COUNT(DISTINCT laComp) = (
		SELECT COUNT(idComp)
		FROM Compagnie
		WHERE estLowCost = 1
	);
	
	-- 3. les noms des pilotes ayant travaillé pour exactement toutes les compagnies low cost

	SELECT DISTINCT nomPilote
	FROM Pilote
	WHERE NOT EXISTS (
		SELECT idComp
		FROM Compagnie
		WHERE estLowCost = 1
		MINUS
		SELECT laComp
		FROM ATravaillePour
		WHERE idPilote = lePilote
	) AND NOT EXISTS (
		SELECT laComp
		FROM ATravaillePour
		WHERE idPilote = lePilote
		MINUS
		SELECT idComp
		FROM Compagnie
		WHERE estLowCost = 1
	);

	SELECT DISTINCT nomPilote
	FROM Pilote, ATravaillePour, Compagnie
	WHERE idPilote = lePilote
	AND laComp = idComp
	AND estLowCost = 1
	GROUP BY nomPilote
	HAVING COUNT(DISTINCT laComp) = (
		SELECT COUNT(idComp)
		FROM Compagnie
		WHERE estLowCost = 1
	)
	INTERSECT
	SELECT DISTINCT nomPilote
	FROM Pilote, ATravaillePour
	WHERE idPilote = lePilote
	GROUP BY nomPilote
	HAVING COUNT(DISTINCT laComp) = (
		SELECT COUNT(idComp)
		FROM Compagnie
		WHERE estLowCost = 1
	)

-- FONCTIONS DE GROUPE

	-- 1. Afficher le nombre de compagnies low cost (de deux manières différentes).

	SELECT COUNT(*)
	FROM Compagnie
	WHERE estLowCost = 1;

	SELECT SUM(estLowCost)
	FROM COMPAGNIE;

	-- 2. Afficher le nombre d’avions appartenant à Air France.

	SELECT COUNT(*)
	FROM Avion, Compagnie
	WHERE compAv = idComp
	AND nomComp = 'Air France';

	-- 3. Afficher le nombre de type d’avions appartenant à Ryanair.

	SELECT COUNT(DISTINCT leTypeAvion)
	FROM Avion, Compagnie
	WHERE compAv = idComp
	AND nomComp = 'Ryanair';

	-- 4. Afficher le nombre total de passagers que peut transporter Air France.

	SELECT SUM(nbPassagers)
	FROM TypeAvion, Avion, Compagnie
	WHERE leTypeAvion = idTypeAvion
	AND compAv = idComp
	AND nomComp = 'Air France';

	-- 5. Afficher le type d’avion ayant la capacité maximale.

	SELECT idTypeAvion
	FROM TypeAvion
	WHERE nbPassagers = (
		SELECT MAX(nbPassagers)
		FROM TypeAvion
	);

	-- 6. Afficher les compagnies (nom) ayant le type d’avion de capacité maximale

	SELECT DISTINCT nomComp
	FROM Compagnie, Avion, TypeAvion
	WHERE idComp = compAv
	AND leTypeAvion = idTypeAvion
	AND nbPassagers = (
		SELECT MAX(nbPassagers)
		FROM TypeAvion
	);

-- REGROUPEMENT

	-- 1. Afficher le(s) pilote(s) ayant au moins deux qualifications.

	SELECT unPilote
	FROM Qualification
	GROUP BY unPilote
	HAVING COUNT(*) >= 2;

	-- 2. Afficher le(s) pilote(s) ayant le plus grand nombre de qualifications.

	SELECT unPilote
	FROM Qualification
	GROUP BY unPilote
	HAVING COUNT(*) = (
		SELECT MAX(nbQualif)
		FROM (
			SELECT unPilote, COUNT(*) AS nbQualif
			FROM Qualification
			GROUP BY unPilote
		)
	);

	-- 3. Afficher le(s) pilote(s) ayant le plus petit nombre de qualifications, éventuellement 0.

	SELECT idPilote
	FROM Pilote
	WHERE NOT EXISTS (
		SELECT unPilote
		FROM Qualification
		WHERE idPilote = unPilote
	);

	-- 4. Pour chaque compagnie désignée par son nom (triée dans l’ordre alphabétique), afficher le nombre d’heures de vol moyen des pilotes.

	SELECT nomComp, AVG(nbHVol)
	FROM Pilote
		RIGHT JOIN Compagnie ON compPil = idComp
	GROUP BY nomComp
	ORDER BY nomComp;

	-- 5. Afficher la compagnie ayant le plus grand nombre de places.

	SELECT compAv
	FROM Compagnie, Avion, TypeAvion
	WHERE idComp = compAv
	AND leTypeAvion = idTypeAvion
	GROUP BY compAv
	HAVING SUM(nbPassagers) = (
		SELECT MAX(SUM(nbPassagers))
		FROM Compagnie, Avion, TypeAvion
		WHERE idComp = compAv
		AND leTypeAvion = idTypeAvion
		GROUP BY nomComp
	);

-- DIVISION

	-- 1. les noms des compagnies possédant tous les types d’Airbus.

	SELECT DISTINCT nomComp
	FROM Compagnie, Avion, TypeAvion
	WHERE idComp = compAv
	AND leTypeAvion = idTypeAvion
	AND idTypeAvion LIKE 'A%'
	AND NOT EXISTS (
		SELECT idTypeAvion
		FROM TypeAvion
		WHERE idTypeAvion LIKE 'A%'
		MINUS
		SELECT leTypeAvion
		FROM Avion
		WHERE compAv = idComp
	);

	SELECT nomComp
	FROM Compagnie, Avion, TypeAvion
	WHERE idComp = compAv
	AND leTypeAvion = idTypeAvion
	AND idTypeAvion LIKE 'A%'
	GROUP BY nomComp
	HAVING COUNT(DISTINCT leTypeAvion) = (
		SELECT COUNT(idTypeAvion)
		FROM TypeAvion
		WHERE idTypeAvion LIKE 'A%'
	);

	-- 2. les noms des compagnies possédant exactement tous les types d’Airbus

	SELECT DISTINCT nomComp
	FROM Compagnie, Avion, TypeAvion
	WHERE idComp = compAv
	AND leTypeAvion = idTypeAvion
	AND idTypeAvion LIKE 'A%'
	AND NOT EXISTS (
		SELECT idTypeAvion
		FROM TypeAvion
		WHERE idTypeAvion LIKE 'A%'
		MINUS
		SELECT leTypeAvion
		FROM Avion
		WHERE compAv = idComp
	) AND NOT EXISTS (
		SELECT leTypeAvion
		FROM Avion
		WHERE compAv = idComp
		MINUS
		SELECT idTypeAvion
		FROM TypeAvion
		WHERE idTypeAvion LIKE 'A%'
	);