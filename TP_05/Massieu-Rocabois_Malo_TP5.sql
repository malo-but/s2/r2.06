-- 1. Créer toutes les vues nécessaires pour gérer ces trois contraintes.

	-- Les comptes qui n'appartiennent à aucun client

		CREATE OR REPLACE VIEW vue_comptes_sans_client
		AS
		SELECT numCompte
		FROM Compte
		MINUS
		SELECT unCompte
		FROM Appartient;

	-- Les agences qui ont plus ou moins de 1 directeur

		CREATE OR REPLACE VIEW vue_agences_mauvais_nombre_directeur
		AS
		SELECT numAgence
		FROM Agence
		WHERE (SELECT COUNT(*)
			FROM Agent
			WHERE sonAgence = numAgence AND directeur = 1) <> 1;

	-- Les agents payés plus que leur directeur

		CREATE OR REPLACE VIEW vue_agents_trop_payes
		AS
		SELECT numAgent, numAgence
		FROM Agent, Agence
		WHERE directeur = 0
		AND sonAgence = numAgence
		AND salaire > (
			SELECT MAX(salaire)
			FROM Agent
			WHERE directeur = 1
		);

-- 2. Créer une vue permettant d’afficher, pour chaque client, son âge et son agence.

	CREATE OR REPLACE VIEW vue_client_age_agence
	AS
	SELECT numClient, (EXTRACT(YEAR FROM SYSDATE) - EXTRACT(YEAR FROM dateNaissanceClient)) AS age, sonAgence
	FROM Client, Agent, Agence
	WHERE sonAgent = numAgent
	AND sonAgence = numAgence;
