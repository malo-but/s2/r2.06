use bd_iut;

DROP TABLE Apprenti;
DROP TABLE Stagiaire;
DROP TABLE Entreprise;
DROP TABLE Etudiant;
DROP TABLE GroupeInfo1;
DROP TABLE Enseignant;


CREATE TABLE Enseignant(

	idEns VARCHAR(4),

	nomEns VARCHAR(20),

	prenomEns VARCHAR(20),

	CONSTRAINT pk_Enseignant PRIMARY KEY(idens)

);

CREATE TABLE GroupeInfo1(

	idGroupe VARCHAR(1),

	tuteurGroupe VARCHAR(4) NOT NULL,

	CONSTRAINT pk_GroupeInfo1 PRIMARY KEY(idGroupe),

	CONSTRAINT fk_GroupeInfo1_Enseignant FOREIGN KEY(tuteurGroupe) REFERENCES Enseignant(idEns),
	CONSTRAINT uq_tuteurGroupe UNIQUE(tuteurGroupe)

);

CREATE TABLE Etudiant(

	idEtud INTEGER,

	nomEtud VARCHAR(20),

	prenomEtud VARCHAR(20),

	sexe VARCHAR(1),

	bac VARCHAR(6),

	nomLycee VARCHAR(50),

	depLycee INTEGER(3),

	leGroupeInfo1 VARCHAR(1),

	parcoursInfo2 VARCHAR(2),

	formationInfo2 VARCHAR(3),

	poursuiteEtudes VARCHAR(50),

	CONSTRAINT pk_Etudiant PRIMARY KEY(idEtud),	
	
	CONSTRAINT fk_Etudiant_GroupeInfo1 FOREIGN KEY(leGroupeInfo1) REFERENCES GroupeInfo1(idGroupe)

);

CREATE TABLE Entreprise(

	idEntreprise INTEGER,

	nomEntreprise VARCHAR(50),

	depEntreprise INTEGER(3),

	CONSTRAINT pk_Entreprise PRIMARY KEY(idEntreprise)

);

CREATE TABLE Stagiaire(

	etudStagiaire INTEGER,

	CONSTRAINT pk_Stagiaire PRIMARY KEY(etudStagiaire),
	CONSTRAINT fk_Stagiaire_Etuiant FOREIGN KEY(etudStagiaire) REFERENCES Etudiant(idEtud),

	entrepriseStage INTEGER NOT NULL,
	CONSTRAINT fk_Stagiaire_Entreprise FOREIGN KEY(entrepriseStage) REFERENCES Entreprise(idEntreprise)

);

CREATE TABLE Apprenti(

	etudApp INTEGER,
	CONSTRAINT pk_Apprenti PRIMARY KEY(etudApp),
	CONSTRAINT fk_Apprenti_Etuiant FOREIGN KEY(etudApp) REFERENCES Etudiant(idEtud),

	entrepriseApp INTEGER NOT NULL,
	CONSTRAINT fk_Apprenti_Entreprise FOREIGN KEY(entrepriseApp) REFERENCES Entreprise(idEntreprise),

	tuteurApp VARCHAR(4) NOT NULL,
	CONSTRAINT fk_Apprenti_Enseignant FOREIGN KEY(tuteurApp) REFERENCES Enseignant(idEns)

);

CREATE TABLE Responsabilite(

	intituleResp VARCHAR(20),

	CONSTRAINT pk_Responsable PRIMARY KEY(intituleResp),

	leResp VARCHAR(4) NOT NULL,
	CONSTRAINT fk_Responsable_Enseignant FOREIGN KEY(leResp) REFERENCES Enseignant(idEns)

);