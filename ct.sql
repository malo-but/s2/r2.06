create database bd_biblio;

create user 'pierre'@'loacalhost' identified by 'mdp_pierre';
create user 'marie'@'localhost' identified by 'mdp_marie';

create role 'assistant';
grant SELECT on bd_biblio.* TO 'assistant';
grant UPDATE, INSERT, DELETE ON db_biblio.Client TO 'assistant';
grant UPDATE, INSERT, DELETE ON db_biblio.Emprunt TO 'assistant';

create role 'gestionnaire';
grant 'assistant' TO 'gestionnaire';
grant UPDATE, INSERT, DELETE ON db_biblio.Auteur TO 'gestionnaire';
grant UPDATE, INSERT, DELETE ON db_biblio.Ouvrage TO 'gestionnaire';

grant 'gestionnaire' TO 'marie'@'loacalhost';
set default role all to 'marie'@'localhost';
grant 'assistant' TO 'pierre'@'localhost';
set default role all to 'pierre'@'localhost';

select nomClient 
from Client
where 