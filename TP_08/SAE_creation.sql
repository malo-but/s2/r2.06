use bd_SAE;

DROP TABLE COMPTAGE;
DROP TABLE COMPTEUR;
DROP TABLE DATEINFO;
DROP TABLE QUARTIER;


CREATE TABLE QUARTIER(
    idQuartier INTEGER,
    nomQuartier VARCHAR(50),
    longeurPisteVelo FLOAT,
    CONSTRAINT pk_Quartier PRIMARY KEY(idQuartier)
);
     
CREATE TABLE COMPTEUR(
    idCompteur INTEGER,
    nomCompteur VARCHAR(50),
    sens VARCHAR(20),
    coord_X FLOAT,
    coord_Y FLOAT,
    leQuartier INTEGER,
    CONSTRAINT pk_Compteur PRIMARY KEY(idCompteur),
    CONSTRAINT fk_Quartier_Compteur FOREIGN KEY(leQuartier) REFERENCEs QUARTIER(idQuartier)
);

CREATE TABLE DATEINFO(
    laDate DATE,
    tempMoy FLOAT,
    jour VARCHAR(10),
    vacances VARCHAR(20),
    CONSTRAINT pk_DateInfo PRIMARY KEY(laDate),
    CONSTRAINT ck_jour CHECK(UPPER(jour) IN('LUNDI','MARDI','MERCREDI','JEUDI','VENDREDI','SAMEDI','DIMANCHE'))
);

CREATE TABLE COMPTAGE(
		leCompteur INTEGER,
        dateComptage DATE,
		h00 INTEGER,
        h01 INTEGER,
        h02 INTEGER,
        h03 INTEGER,
        h04 INTEGER,
        h05 INTEGER,
        h06 INTEGER,
        h07 INTEGER,
        h08 INTEGER,
        h09 INTEGER,
        h10 INTEGER,
        h11 INTEGER,
        h12 INTEGER,
        h13 INTEGER,
        h14 INTEGER,
        h15 INTEGER,
        h16 INTEGER,
        h17 INTEGER,
        h18 INTEGER,
        h19 INTEGER,
        h20 INTEGER,
        h21 INTEGER,
        h22 INTEGER,
        h23 INTEGER,
        presenceAnomalie VARCHAR(10),
        CONSTRAINT ck_anomalie CHECK(UPPER(presenceAnomalie) IN('FORTE','FAIBLE')),
        CONSTRAINT fk_Comptage_Compteur FOREIGN KEY(leCompteur) REFERENCEs COMPTEUR(idCompteur),
        CONSTRAINT fk_Comptage_dateInfo FOREIGN KEY(dateComptage) REFERENCEs DATEINFO(laDate),
        CONSTRAINT pk_Comptage PRIMARY KEY (leCompteur,dateComptage)
);
