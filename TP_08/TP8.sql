/*
bd_SAE :

QUARTIER(idQuartier(1), nomQuartier, longeurPisteVelo)
COMPTEUR(idCompteur(1), nomCompteur, sens, coord_X, coord_Y, leQuartier=QUARTIER.idQuartier)
DATEINFO(laDate(1), tempMoy, jour, vacances)
COMPTAGE(leCompteur(1)=COMPTEUR.idCompteur, dateComptage(1)=DATEINFO.laDate, h00, h01, h02, h03, h04, h05, h06, h07, h08, h09, h10, h11, h12, h13, h14, h15, h16, h17, h18, h19, h20, h21)

*/

-- 1. Afficher les compteurs sans quartier associé.

SELECT * 
FROM COMPTEUR 
WHERE leQuartier IS NULL;

-- 2. Afficher les compteurs ayant un "sens" différent des 4 points cardinaux.

SELECT idCompteur, sens
FROM COMPTEUR 
WHERE UPPER(sens) NOT IN ('NORD', 'SUD', 'EST', 'OUEST');

-- 3. Pour chaque nom de compteur, afficher le nombre de compteurs et le nombre de "sens" distincts.

SELECT nomCompteur, COUNT(idCompteur) AS nbCompteurs, COUNT(DISTINCT sens) AS nbSens 
FROM COMPTEUR 
GROUP BY nomCompteur;

-- 4. Afficher les quartiers désignés par leur nom dans l’ordre décroissant du nombre de compteurs.

SELECT nomQuartier, COUNT(idCompteur) AS nbCompteurs
FROM QUARTIER
LEFT JOIN COMPTEUR ON QUARTIER.idQuartier = COMPTEUR.leQuartier
GROUP BY nomQuartier
ORDER BY nbCompteurs DESC;

-- 5. Afficher le(s) quartier(s) avec le minimum de compteurs.
 
SELECT nomQuartier, COUNT(idCompteur) AS nbCompteurs
FROM QUARTIER
LEFT JOIN COMPTEUR ON QUARTIER.idQuartier = COMPTEUR.leQuartier
GROUP BY idQuartier, nomQuartier
HAVING COUNT(idCompteur) = (
	SELECT MIN(nbCompt)
	FROM (
		SELECT COUNT(idCompteur) AS nbCOmpt
		FROM QUARTIER
		LEFT JOIN COMPTEUR ON QUARTIER.idQuartier = COMPTEUR.leQuartier
		GROUP BY idQuartier, nomQuartier
	) nbCompteursParQuartier
);

-- 6. Afficher la distribution de la variable "vacances" au regard du nombre de dates enregistrées.

SELECT vacances, COUNT(*) AS nbDates
FROM DATEINFO
GROUP BY vacances;

-- 7. En 2022, afficher pour chacune des saisons la température minimale, maximale et moyenne.

CREATE OR REPLACE VIEW vue_saison AS
SELECT laDate, tempMoy, jour, vacances, 'hiver' saison FROM DATEINFO WHERE laDate BETWEEN '2022-01-01' AND '2022-03-20' OR laDate BETWEEN '2022-12-21' AND '2022-12-31'
UNION
SELECT laDate, tempMoy, jour, vacances, 'printemps' saison FROM DATEINFO WHERE laDate BETWEEN '2022-03-21' AND '2022-06-20'
UNION
SELECT laDate, tempMoy, jour, vacances, 'été' saison FROM DATEINFO WHERE laDate BETWEEN '2022-06-21' AND '2022-09-20'
UNION
SELECT laDate, tempMoy, jour, vacances, 'automne' saison FROM DATEINFO WHERE laDate BETWEEN '2022-09-21' AND '2022-12-20';

SELECT * FROM vue_saison;

SELECT saison, COUNT(laDate) nbJours, ROUND(AVG(tempMoy), 3) tempMoyenne, MIN(tempMoy) tempMin, MAX(tempMoy) tempMax
FROM vue_saison
GROUP BY saison;

-- 8. En 2021, hors vacances, afficher pour chaque jour de la semaine, le nombre de vélos.

SELECT jour, SUM(h00 + h01 + h02 + h03 + h04 + h05 + h06 + h07 + h08 + h09 + h10 + h11 + h12 + h13 + h14 + h15 + h16 + h17 + h18 + h19 + h20 + h21) AS nbVelo
FROM DATEINFO
LEFT JOIN COMPTAGE ON DATEINFO.laDate = COMPTAGE.dateComptage
WHERE YEAR(laDate) = 2021 AND vacances IS NULL
GROUP BY jour;

-- 9. En 2021, pendant les vacances, afficher pour chaque jour de la semaine, le nombre de vélos.

SELECT jour, SUM(h00 + h01 + h02 + h03 + h04 + h05 + h06 + h07 + h08 + h09 + h10 + h11 + h12 + h13 + h14 + h15 + h16 + h17 + h18 + h19 + h20 + h21) AS nbVelo
FROM DATEINFO
LEFT JOIN COMPTAGE ON DATEINFO.laDate = COMPTAGE.dateComptage
WHERE YEAR(laDate) = 2021 AND vacances IS NOT NULL
GROUP BY jour;

-- 10. Pour chaque quartier, afficher le nombre d’anomalies "fortes".

SELECT nomQuartier, COUNT(presenceAnomalie) AS nbAnomalie
FROM QUARTIER
LEFT JOIN COMPTEUR ON QUARTIER.idQuartier = COMPTEUR.leQuartier
LEFT JOIN COMPTAGE ON COMPTEUR.idCompteur = COMPTAGE.leCompteur
WHERE presenceAnomalie = 'forte'
GROUP BY nomQuartier;