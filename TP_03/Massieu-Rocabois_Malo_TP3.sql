-- 1. Pour chaque tuteur d’apprenti désigné par son nom (trié dans l’ordre alphabétique), afficher le nombre d’apprentis.

SELECT nomEns, COUNT(etudApp)
FROM Apprenti, Enseignant
WHERE tuteurApp = idEns
GROUP BY nomEns
ORDER BY nomEns;

-- 2. Pour chaque enseignant désigné par son nom, afficher le nombre d’apprentis (éventuellement 0) dans l’ordre décroissant.

SELECT nomEns, COUNT(etudApp) AS nbApprentis
FROM Enseignant 
	LEFT JOIN Apprenti ON tuteurApp = idEns
GROUP BY nomEns
ORDER BY nbApprentis DESC;

-- 3. Afficher le plus grand nombre d’apprentis suivis par un tuteur.

SELECT MAX(nbApprentis)
FROM (
	SELECT nomEns, COUNT(etudApp) AS nbApprentis
	FROM Apprenti, Enseignant
	WHERE tuteurApp = idEns
	GROUP BY nomEns
);

-- 4. Afficher le(s) tuteur(s) ayant le plus grand nombre d’apprentis.

SELECT nomEns
FROM Apprenti, Enseignant
WHERE tuteurApp = idEns
GROUP BY nomEns
HAVING COUNT(etudApp) = (
	SELECT MAX(nbApprentis) AS maxApprentis
	FROM (
		SELECT nomEns, COUNT(etudApp) AS nbApprentis
		FROM Apprenti, Enseignant
		WHERE tuteurApp = idEns
		GROUP BY nomEns
	)
);

-- 5. Afficher le nombre moyen d’apprentis par tuteur.

SELECT AVG(COUNT(etudApp))
FROM Apprenti 
GROUP BY tuteurApp;

-- 6. Afficher les tuteurs ayant un nombre d’apprentis strictement supérieur à la moyenne.

SELECT nomEns
FROM Apprenti, Enseignant
WHERE tuteurApp = idEns
GROUP BY nomEns
HAVING COUNT(etudApp) > (
	SELECT AVG(COUNT(etudApp))
	FROM Apprenti 
	GROUP BY tuteurApp;
);

-- 7. Pour chaque département, afficher le nombre de lycéens recrutés en première année dans l’ordre décroissant.

SELECT depLycee, COUNT(depLycee) AS nbLycéens
FROM Etudiant
WHERE depLycee IS NOT NULL
GROUP BY depLycee
ORDER BY COUNT(depLycee) DESC;

-- 8. Afficher le département de Bretagne qui a fourni le moins de lycéens recrutés en première année.

SELECT depLycee
FROM Etudiant
WHERE depLycee IN (22, 29, 35, 56)
GROUP BY depLycee
HAVING COUNT(depLycee) = (
  SELECT MIN(COUNT(depLycee))
  FROM Etudiant
  WHERE depLycee IN (22, 29, 35, 56)
  GROUP BY depLycee
);

-- 9. Afficher les poursuites d’études concernant au moins 5 étudiants.

SELECT poursuiteEtudes, COUNT(poursuiteEtudes) AS nbEtudiants
FROM Etudiant
WHERE poursuiteEtudes IS NOT NULL
GROUP BY poursuiteEtudes
HAVING COUNT(poursuiteEtudes) >= 5;

-- 10. Afficher la poursuite d’études ayant le maximum d’étudiants apprentis.

SELECT poursuiteEtudes
FROM Etudiant
WHERE poursuiteEtudes IS NOT NULL
GROUP BY poursuiteEtudes
HAVING COUNT(poursuiteEtudes) = (
	SELECT MAX(COUNT(poursuiteEtudes))
	FROM Etudiant
	WHERE poursuiteEtudes IS NOT NULL
	GROUP BY poursuiteEtudes
);

-- 11. Afficher les 3 poursuites d’études les plus représentées parmi les étudiants du parcours Développeurs d’Applications (DA).

SELECT poursuiteEtudes, COUNT(poursuiteEtudes) AS nbEtudiants
FROM Etudiant
WHERE poursuiteEtudes IS NOT NULL 
AND parcoursInfo2 = 'DA'
GROUP BY poursuiteEtudes
ORDER BY COUNT(poursuiteEtudes) DESC
FETCH FIRST 3 ROWS ONLY;