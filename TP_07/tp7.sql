------------------------------
-- Shell / root@localhost --
------------------------------
-- Q1

DROP USER malo@localhost;
DROP DATABASE db_malo;

-- Q2

CREATE DATABASE bd_iut;
CREATE USER 'naert'@'localhost' IDENTIFIED BY 'mdp_naert';
GRANT ALL PRIVILEGES ON *.* TO 'naert'@'localhost' WITH GRANT OPTION;

-- Q3

connect bd_iut;
source ~/BUT/S2/R2.06/TP_07/creation.sql;
source ~/BUT/S2/R2.06/TP_07/remplissage.sql;

------------------------------------------
-- Workbench / naert@localhost --
------------------------------------------

-- Q4

CREATE VIEW vue_Etud_Stag_Ent AS
SELECT * 
FROM ETUDIANT, STAGIAIRE, ENTREPRISE
WHERE ETUDIANT.idEtud = STAGIAIRE.etudStagiaire
AND STAGIAIRE.entrepriseStage = ENTREPRISE.idEntreprise
WITH CHECK OPTION;

CREATE VIEW vue_Etud_App_Ent AS
SELECT * 
FROM ETUDIANT, APPRENTI, ENTREPRISE
WHERE ETUDIANT.idEtud = APPRENTI.etudApp
AND APPRENTI.entrepriseApp = ENTREPRISE.idEntreprise
WITH CHECK OPTION;

-- Q5

CREATE USER 'kamp'@'localhost' IDENTIFIED BY 'mdp_kamp';

CREATE USER 'ridard'@'localhost' IDENTIFIED BY 'mdp_ridard';

CREATE USER 'baudont'@'localhost' IDENTIFIED BY 'mdp_baudont';

CREATE USER 'fleurquin'@'localhost' IDENTIFIED BY 'mdp_fleurquin';

-- Q6

CREATE ROLE 'bd_iut_lecture';
GRANT SELECT ON bd_iut.* TO 'bd_iut_lecture';

CREATE ROLE 'bd_iut_ecriture';
GRANT INSERT, UPDATE, DELETE ON bd_iut.* TO 'bd_iut_ecriture';

-- Q7

GRANT 'bd_iut_lecture', 'bd_iut_ecriture' TO 'kamp'@'localhost';

GRANT 'bd_iut_lecture' TO 'ridard'@'localhost';
GRANT INSERT, UPDATE, DELETE ON bd_iut.ETUDIANT TO 'ridard'@'localhost';
GRANT INSERT, UPDATE, DELETE ON bd_iut.GROUPEINFO1 TO 'ridard'@'localhost';

GRANT 'bd_iut_lecture' TO 'baudont'@'localhost';
GRANT INSERT, UPDATE, DELETE ON bd_iut.vue_Etud_App_Ent TO 'baudont'@'localhost';
GRANT INSERT, UPDATE, DELETE ON bd_iut.ETUDIANT TO 'baudont'@'localhost';
GRANT INSERT, UPDATE, DELETE ON bd_iut.APPRENTI TO 'baudont'@'localhost';
GRANT INSERT, UPDATE, DELETE ON bd_iut.ENTREPRISE TO 'baudont'@'localhost';

GRANT 'bd_iut_lecture' TO 'fleurquin'@'localhost';
GRANT UPDATE(POURSUITEETUDES) ON bd_iut.ETUDIANT TO 'fleurquin'@'localhost';

SET DEFAULT ROLE ALL TO 'kamp'@'localhost';
SET DEFAULT ROLE ALL TO 'ridard'@'localhost';
SET DEFAULT ROLE ALL TO 'baudont'@'localhost';
SET DEFAULT ROLE ALL TO 'fleurquin'@'localhost';

-- Q8

SELECT * FROM mysql.tables_priv;

------------------------------------------
-- Shell / naert@localhost --
------------------------------------------

SHOW GRANTS FOR 'kamp'@'localhost';
SHOW GRANTS FOR 'ridard'@'localhost';
SHOW GRANTS FOR 'baudont'@'localhost';
SHOW GRANTS FOR 'fleurquin'@'localhost';

-- Q9 : Écrire un script de test, personnel et commenté, le plus complet possible.

-- On se connecte en tant que kamp
SYSTEM mysql -u kamp -pmdp_kamp
-- On vérifie la bonne connexion à l'utilisateur
SELECT USER();
-- On se connecte à la base de données bd_iut
USE bd_iut;
-- On affiche la liste des tables
SHOW TABLES;
-- On affiche la table des enseigants
SELECT * FROM ENSEIGNANT;
-- On ajoute un enseignant
INSERT INTO ENSEIGNANT VALUES ('AK', 'Dupont', 'Jean');
-- On affiche l'enseignant ajouté
SELECT * FROM ENSEIGNANT WHERE idEns = 'AK';

-- On se connecte en tant que ridard
SYSTEM mysql -u ridard -pmdp_ridard
-- On vérifie la bonne connexion à l'utilisateur
SELECT USER();
-- On se connecte à la base de données bd_iut
USE bd_iut;
-- On affiche la table GroupeInfo1
SELECT * FROM GROUPEINFO1;
-- On ajoute un groupe
INSERT INTO GROUPEINFO1 VALUES ('E', 'AK');
-- On affiche le groupe ajouté
SELECT * FROM GROUPEINFO1 WHERE idGroupe = 'E';
-- On essaye de supprimer un enseignant
DELETE FROM ENSEIGNANT WHERE idEns = 'AK';
-- On obtient une erreur car ridard n'a pas les droits pour l'écriture sur la table ENSEIGNANT

-- On se connecte en tant que baudont
SYSTEM mysql -u baudont -pmdp_baudont
-- On vérifie la bonne connexion à l'utilisateur
SELECT USER();
-- On se connecte à la base de données bd_iut
USE bd_iut;
-- On affiche la table RESONSABILITE
SELECT * FROM RESPONSABILITE;
-- On modifie le nom d'une entreprise
UPDATE ENTREPRISE SET nomEntreprise = 'Google' WHERE idEntreprise = '1';
-- On affiche l'entreprise modifiée
SELECT * FROM ENTREPRISE WHERE idEntreprise = '1';
-- On essaye d'ajouter une responsabilité
INSERT INTO RESPONSABILITE VALUES ('ASTEP', 'AK');
-- On obtient une erreur car baudont n'a pas les droits pour l'écriture sur la table RESPONSABILITE

-- On se connecte en tant que fleurquin
SYSTEM mysql -u fleurquin -pmdp_fleurquin
-- On vérifie la bonne connexion à l'utilisateur
SELECT USER();
-- On se connecte à la base de données bd_iut
USE bd_iut;
-- On affiche la table APPRENTI
SELECT * FROM APPRENTI;
-- On modifie la poursuite d'étude d'un etudiant
UPDATE ETUDIANT SET poursuiteEtudes = 'ENSAT' WHERE idEtud = 21900754;
-- On affiche l'étudiant modifié
SELECT * FROM ETUDIANT WHERE idEtud = 21900754;
-- On essaye de modifier le nom du lycée d'un etudiant
UPDATE ETUDIANT SET nomLycee = 'Lycée Notre-Dame' WHERE idEtud = 21900754;
-- On obtient une erreur car fleurquin n'a pas les droits pour l'écriture sur l'attribut nomLycee de la table ETUDIANT