use bd_iut;

DROP TABLE APPRENTI;
DROP TABLE STAGIAIRE;
DROP TABLE ENTREPRISE;
DROP TABLE ETUDIANT;
DROP TABLE GROUPEINFO1;
DROP TABLE RESPONSABILITE;
DROP TABLE ENSEIGNANT;


CREATE TABLE ENSEIGNANT(

	idEns VARCHAR(4),

	nomEns VARCHAR(20),

	prenomEns VARCHAR(20),

	CONSTRAINT pk_Enseignant PRIMARY KEY(idens)

);

CREATE TABLE GROUPEINFO1(

	idGroupe VARCHAR(1),

	tuteurGroupe VARCHAR(4) NOT NULL,

	CONSTRAINT pk_GroupeInfo1 PRIMARY KEY(idGroupe),

	CONSTRAINT fk_GroupeInfo1_Enseignant FOREIGN KEY(tuteurGroupe) REFERENCES ENSEIGNANT(idEns),
	CONSTRAINT uq_tuteurGroupe UNIQUE(tuteurGroupe)

);

CREATE TABLE ETUDIANT(

	idEtud INTEGER,

	nomEtud VARCHAR(20),

	prenomEtud VARCHAR(20),

	sexe VARCHAR(1),

	bac VARCHAR(6),

	nomLycee VARCHAR(50),

	depLycee INTEGER(3),

	leGroupeInfo1 VARCHAR(1),

	parcoursInfo2 VARCHAR(2),

	formationInfo2 VARCHAR(3),

	poursuiteEtudes VARCHAR(50),

	CONSTRAINT pk_Etudiant PRIMARY KEY(idEtud),	
	
	CONSTRAINT fk_Etudiant_GroupeInfo1 FOREIGN KEY(leGroupeInfo1) REFERENCES GROUPEINFO1(idGroupe)

);

CREATE TABLE ENTREPRISE(

	idEntreprise INTEGER,

	nomEntreprise VARCHAR(50),

	depEntreprise INTEGER(3),

	CONSTRAINT pk_Entreprise PRIMARY KEY(idEntreprise)

);

CREATE TABLE STAGIAIRE(

	etudStagiaire INTEGER,

	CONSTRAINT pk_Stagiaire PRIMARY KEY(etudStagiaire),
	CONSTRAINT fk_Stagiaire_Etuiant FOREIGN KEY(etudStagiaire) REFERENCES ETUDIANT(idEtud),

	entrepriseStage INTEGER NOT NULL,
	CONSTRAINT fk_Stagiaire_Entreprise FOREIGN KEY(entrepriseStage) REFERENCES ENTREPRISE(idEntreprise)

);

CREATE TABLE APPRENTI(

	etudApp INTEGER,
	CONSTRAINT pk_Apprenti PRIMARY KEY(etudApp),
	CONSTRAINT fk_Apprenti_Etuiant FOREIGN KEY(etudApp) REFERENCES ETUDIANT(idEtud),

	entrepriseApp INTEGER NOT NULL,
	CONSTRAINT fk_Apprenti_Entreprise FOREIGN KEY(entrepriseApp) REFERENCES ENTREPRISE(idEntreprise),

	tuteurApp VARCHAR(4) NOT NULL,
	CONSTRAINT fk_Apprenti_Enseignant FOREIGN KEY(tuteurApp) REFERENCES ENSEIGNANT(idEns)

);

CREATE TABLE RESPONSABILITE(

	intituleResp VARCHAR(20),

	CONSTRAINT pk_Responsable PRIMARY KEY(intituleResp),

	leResp VARCHAR(4) NOT NULL,
	CONSTRAINT fk_Responsable_Enseignant FOREIGN KEY(leResp) REFERENCES ENSEIGNANT(idEns)

);