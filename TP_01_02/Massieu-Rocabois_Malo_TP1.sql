/*
R2.06 - TP1 - 08 / 02 / 2023
MASSIEU--ROCABOIS Malo - Groupe D
*/

/*

Schema relationnel :

Enseignant(
	idEns(1), 
	nomEns, 
	prenomEns
)

GroupeInfo1(
	idGroupe(1), 
	tuteurGroupe = @Enseignant(idEns)(NN)(UQ)
)

Etudiant(
	idEtud(1), 
	nomEtud, 
	prenomEtud, 
	sexe, 
	bac, 
	nomLycee, 
	depLycee, 
	leGroupeInfo1 = @GroupeInfo1(idGroupe),
	parcoursInfo2,
	formationInfo2,
	poursuiteEtudes
)

Entreprise(
	idEntreprise(1),
	nomEntreprise,
	depEntreprise
)

Stagiaire(
	etudStagiaire = @Etudiant(idEtud)(1),
	entrepriseStage = @Entreprise(idEntreprise)(NN)
)

Apprenti(
	etudApp = @Etudiant(idEtud)(1),
	entrepriseApp = @Entreprise(idEntreprise)(NN),
	tuteurApp = @Enseignant(idEns)(NN)
)

*/

DROP TABLE Apprenti;
DROP TABLE Stagiaire;
DROP TABLE Entreprise;
DROP TABLE Etudiant;
DROP TABLE GroupeInfo1;
DROP TABLE Enseignant;


CREATE TABLE Enseignant(

	idEns VARCHAR2(4)	
		CONSTRAINT pk_Enseignant PRIMARY KEY,

	nomEns VARCHAR(20),

	prenomEns VARCHAR2(20)

);

CREATE TABLE GroupeInfo1(

	idGroupe VARCHAR2(1)
		CONSTRAINT pk_GroupeInfo1 PRIMARY KEY,

	tuteurGroupe VARCHAR2(4)
		CONSTRAINT fk_GroupeInfo1_Enseignant REFERENCES Enseignant(idEns)
		CONSTRAINT nn_tuteurGroupe NOT NULL
		CONSTRAINT uq_tuteurGroupe UNIQUE

);

CREATE TABLE Etudiant(

	idEtud NUMBER
		CONSTRAINT pk_Etudiant PRIMARY KEY,

	nomEtud VARCHAR2(20),

	prenomEtud VARCHAR2(20),

	sexe VARCHAR2(1),

	bac VARCHAR(6),

	nomLycee VARCHAR2(50),

	depLycee NUMBER(3),

	leGroupeInfo1 VARCHAR2(1)
		CONSTRAINT fk_Etudiant_GroupeInfo1 REFERENCES GroupeInfo1(idGroupe),

	parcoursInfo2 VARCHAR2(2),

	formationInfo2 VARCHAR2(3),

	poursuiteEtudes VARCHAR2(50)

);

CREATE TABLE Entreprise(

	idEntreprise NUMBER
		CONSTRAINT pk_Entreprise PRIMARY KEY,

	nomEntreprise VARCHAR2(50),

	depEntreprise NUMBER(3)

);

CREATE TABLE Stagiaire(

	etudStagiaire NUMBER 
		CONSTRAINT pk_Stagiaire PRIMARY KEY
		CONSTRAINT fk_Stagiaire_Etuiant REFERENCES Etudiant(idEtud),

	entrepriseStage NUMBER
		CONSTRAINT nn_entrepriseStagiaire NOT NULL
		CONSTRAINT fk_Stagiaire_Entreprise REFERENCES Entreprise(idEntreprise)

);

CREATE TABLE Apprenti(

	etudApp NUMBER
		CONSTRAINT pk_Apprenti PRIMARY KEY
		CONSTRAINT fk_Apprenti_Etuiant REFERENCES Etudiant(idEtud),

	entrepriseApp NUMBER
		CONSTRAINT nn_entrepriseApp NOT NULL
		CONSTRAINT fk_Apprenti_Entreprise REFERENCES Entreprise(idEntreprise),

	tuteurApp VARCHAR2(4)
		CONSTRAINT nn_tuteurApp NOT NULL
		CONSTRAINT fk_Apprenti_Enseignant REFERENCES Enseignant(idEns)

);