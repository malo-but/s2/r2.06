/*
R2.06 - TP1 - 08 / 02 / 2023
MASSIEU--ROCABOIS Malo - Groupe D
*/

-- 1. Pour chaque stagiaire (id), afficher le nom et le département de l’entreprise.

SELECT etudStagiaire, nomEntreprise, depEntreprise 
FROM Entreprise, Stagiaire 
WHERE entrepriseStage = idEntreprise;

/*

ETUDSTAGIAIRE	NOMENTREPRISE							DEPENTREPRISE
21903040		ACCENTURE								75
21900078		ACCENTURE								75
21902500		ADAPEI56								56
21900834		ADM										56
21902446		APRAS									35
21900858		BYSTAMP									56
21900316		CADIOU INDUSTRIE						29
21902833		CAISSE DEPARGNE LOIRE DROME ARDECHE		42
21802862		CGI										56
21900422		CG									I	56


58 rows selected.

*/

-- 2. Pour chaque stagiaire (nom et prénom), afficher le nom et le département de l’entreprise.

SELECT nomEtud, prenomEtud, nomEntreprise, depEntreprise
FROM Etudiant, Entreprise, Stagiaire
WHERE etudStagiaire = idEtud AND entrepriseStage = idEntreprise;

/*

NOMETUD		PRENOMETUD	NOMENTREPRISE				DEPENTREPRISE
MAHEO		CORENTIN	COLLEGE SACRE COEUR			56
MAINDRON	ELOUAN		DEPARTEMENT DE LA VENDEE	85
MEAUZE		BAPTISTE	LACTALIS					53
MERIC-PONS	MATHIS		CGI							56
MICHENEAU	TIMOTHE		ERCII						56
MIONET		PIERRE		NAVAL GROUP					83
MULTAN		GABRIELLE	DAWIZZ						56
PEDRON		MATISSE		CGI							56
QUACH		VALENTIN	PLASTEOL					56

58 rows selected.

*/

-- 3. Pour chaque étudiant (id), afficher son entreprise (nom) si elle existe et rien sinon.

SELECT idEtud, nomEntreprise
FROM Etudiant
	LEFT JOIN (
		SELECT etudStagiaire idEtudEnt, nomEntreprise
		FROM Stagiaire, Entreprise
		WHERE entrepriseStage = idEntreprise	
		UNION
		SELECT etudApp idEtudEnt, nomEntreprise
		FROM Apprenti, Entreprise
		WHERE entrepriseApp = idEntreprise
	)
	ON idEtud = idEtudEnt;

/*

IDETUD		NOMENTREPRISE
21702527	SCM VIAUD-FORMAGNE
21802862	CGI
21807519	ANVERGUR
21900078	ACCENTURE
21900144	IRISA, EQUIPE OBELIX
21900162	MENBAT
21900169	EXOTIC ANIMALS
21900219	MENBAT
21900234	YOGOKO
21900238	SMARTMOOV

107 rows selected.

*/

-- 4. Afficher les étudiants (nom et prénom) ayant réalisé un stage dans un département différent de celui du lycée.

SELECT nomEtud, prenomEtud
FROM Etudiant, Stagiaire, Entreprise
WHERE etudStagiaire = idEtud 
AND entrepriseStage = idEntreprise 
AND depEntreprise != depLycee;

/*

NOMETUD		PRENOMETUD
AUVRAY		ALEXANDRE
BEAUCLAIR	ADRIEN
BERNIER		ALLAN
BONNET		BENJAMIN
BRAUD		ANTOINE
BUCHE		SYLVAIN
CALVEZ		LOUIS
CHIFFOT		CALVIN
CHOLLET		QUENTIN
COBAT		GUILLAUME

31 rows selected.

*/

--  5. Afficher les apprentis (nom et prénom) suivis par Muriel Mannevy.

SELECT nomEtud, prenomEtud
FROM Etudiant, Apprenti, Enseignant
WHERE etudApp = idEtud
AND tuteurApp = idEns
AND UPPER(nomEns) = 'MANNEVY'
AND UPPER(prenomEns) = 'MURIEL';

/*

NOMETUD		PRENOMETUD
KOENIGS		THEO
LE BRETON	DAN
LE PORS		YANIS
MADELAINE	DYLAN
PLOQUIN		NATHAN
ROUILLIER	JULIEN
TIRLEMONT	KIERIAN

7 rows selected.

*/

-- 6. Pour chaque tuteur d’apprenti (nom), afficher les apprentis (nom et prénom).

SELECT nomEns, nomEtud, prenomEtud
FROM Enseignant, Apprenti, Etudiant
WHERE tuteurApp = idEns
AND etudApp = idEtud;

/*

NOMENS	NOMETUD			PRENOMETUD
Tuffigo	ADAM			ANTOINE
Baudont	BREIT HOARAU	EMELINE
Lefevre	CARN			YOHAN
Lefevre	CASTELLA		MATEO
Baudont	CLOAREC			THOMAS
Tuffigo	DESMONTS		LEO
Baudont	DUFILS			ROMAIN
Ridard	FAUGERON		LUCAS

24 rows selected.

*/

-- 7. Afficher les tuteurs (nom) ayant suivi un apprenti en dehors de la Bretagne.

SELECT nomEns
FROM Enseignant, Apprenti, Entreprise
WHERE tuteurApp = idEns
AND entrepriseApp = idEntreprise
AND depEntreprise != '29'
AND depEntreprise != '22'
AND depEntreprise != '35'
AND depEntreprise != '56';

/*

NOMENS
Mannevy
Baudont

2 rows selected.

*/

-- 8. Pour chaque enseignant (nom), afficher les apprentis (nom et prénom) en Bretagne et le département, triés dans l’ordre décroissant du département, s’ils existent et rien sinon.

SELECT nomENS, nomEtud, prenomEtud, depEntreprise
FROM Enseignant
	LEFT JOIN (
		SELECT tuteurApp idEnsEnt, nomEtud, prenomEtud, depEntreprise
		FROM Apprenti, Etudiant, Entreprise
		WHERE etudApp = idEtud
		AND entrepriseApp = idEntreprise
		AND (depEntreprise = '29' OR depEntreprise = '22' OR depEntreprise = '35' OR depEntreprise = '56')
	)
	ON idEns = idEnsEnt
ORDER BY depEntreprise DESC;

/*

NOMENS		NOMETUD		PRENOMETUD	DEPENTREPRISE
Merciol		-			- 	 		- 
Joucla		 - 			- 	 		- 
Le Sommer	 - 		 	- 	 		- 
Borne		 - 		 	- 	 		- 
Godin		 - 		 	- 	 		- 
Lesueur		 - 		 	- 	 		- 
Roirand		 - 		 	- 	 		- 
Naert		 - 		 	- 	 		- 

35 rows selected.

*/

-- 9. Afficher les étudiants (nom et prénom) qui ont travaillé dans le Morbihan et qui ont poursuivi leurs études à l’ENSIBS.

SELECT DISTINCT UPPER(nomEtud), UPPER(prenomEtud)
FROM Etudiant, Stagiaire, Apprenti, Entreprise
WHERE ((etudStagiaire = idEtud AND entrepriseStage = idEntreprise)
OR (etudApp = idEtud AND entrepriseApp = idEntreprise))
AND depEntreprise = 56
AND UPPER(poursuiteEtudes) LIKE '%ENSIBS%';

/*


UPPER(NOMETUD)	UPPER(PRENOMETUD)
GARIN-HAMELINE	GILDAS
PEDRON			MATISSE
LE GARREC		VINCENT
LE CHENADEC		SARAH
GUENNEC			PAUL

5 rows selected.

*/

-- 10. Afficher les enseignants (nom) à la fois tuteur de groupe en Info1 et tuteur d’apprenti.

SELECT DISTINCT UPPER(nomEns)
FROM Enseignant, GroupeInfo1, Apprenti
WHERE idEns = tuteurGroupe
AND idEns = tuteurApp;

/*


UPPER(NOMENS)
TUFFIGO


*/

-- 11. Afficher le nombre d’étudiants venant d’un lycée breton.

SELECT COUNT(*) 
FROM Etudiant
WHERE depLycee IN (22, 29, 35, 56);

/*

COUNT(*)
81

*/

-- 12. Afficher le nombre de poursuites d’études renseignées.

SELECT COUNT(*)
FROM Etudiant
WHERE poursuiteEtudes IS NOT NULL;

/*

COUNT(*)
74

*/

-- 13. Afficher le nombre de tuteurs pour l’ensemble des apprentis.

SELECT COUNT(DISTINCT tuteurApp)
FROM Apprenti;

SELECT COUNT(DISTINCT idEns)
FROM Enseignant, Apprenti
WHERE idEns = tuteurApp;

SELECT COUNT(*)
FROM (
	SELECT DISTINCT idEns
	FROM Enseignant, Apprenti
	WHERE idEns = tuteurApp
);

/*

COUNT(*)
7

*/

-- 14. Afficher le nombre d’apprentis suivis par Pascal Baudont dans le Morbihan.

SELECT COUNT(*)
FROM Apprenti, Enseignant, Entreprise
WHERE tuteurApp = idEns
AND UPPER(prenomEns) = 'PASCAL'
AND UPPER(nomEns) = 'BAUDONT'
AND entrepriseApp = idEntreprise
AND depEntreprise = 56;

/*

COUNT(*)
4

*/

-- 15. Afficher la proportion d’étudiants passés en deuxième année.

SELECT COUNT(*) / COUNT(parcoursInfo2)
FROM Etudiant;

/*

COUNT(*)/COUNT(PARCOURSINFO2)
1.30487804878048780487804878048780487805

*/

-- 16. Afficher la proportion d’enseignants ayant suivi un apprenti.

SELECT COUNT(DISTINCT tuteurApp) / COUNT(DISTINCT idEns)
FROM Enseignant, Apprenti;

/*

COUNT(DISTINCTTUTEURAPP)/COUNT(DISTINCTIDENS)
.35

*/